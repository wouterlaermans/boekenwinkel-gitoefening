<?php

$pdo = new PDO('mysql:host=localhost;dbname=boekenwinkel', 'root', '');
$query = $pdo->prepare("SELECT * FROM boeken WHERE id =:id");
$query->execute(['id' => ...]);
$boeken = $query->fetchAll();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Overzicht boeken - Boekwinkel Laermans</title>
</head>
<body>

    <header class="main-header">
        <h1>Boekenwinkel Laermans</h1>
    </header>

    <main>
        <h1>Boek x</h1>
    </main>
    
</body>
</html>